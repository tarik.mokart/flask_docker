import json
from flask import request, jsonify
from flask_api import status
from . import create_app
from database.models import *
from database.schemas import *



app = create_app(db)
print("app created")


@app.route('/')
def hello_world():
    return 'Hello, Docker! This is our APP ! '

#############  GET ENDPOINTS ###########

@app.route('/rexs', methods=['GET'])
def fetch_rexs():
    rexs = Rexs.query.all()
    return jsonify({"data" : rexs_schema.dump(rexs)})

@app.route('/rexs/<id>', methods=['GET'])
def fetch_rex(id):
    rex = Rexs.query.get(id)
    return jsonify({"data" : rex_schema.dump(rex)})


@app.route('/solutions', methods=['GET'])
def fetch_solutions():
    solutions = Solutions.query.all()
    return jsonify({"data" : solutions_schema.dump(solutions)})

@app.route('/solutions/<id>', methods=['GET'])
def fetch_solution(id):
    solution = Solutions.query.get(id)
    return jsonify({"data" : solution_schema.dump(solution)})


@app.route('/usecases', methods=['GET'])
def fetch_usecases():
    usecases = Usecases.query.all()
    return jsonify({"data" : usecases_schema.dump(usecases)})

@app.route('/usecases/<id>', methods=['GET'])
def fetch_usecase(id):
    usecase = Usecases.query.get(id)
    return jsonify({"data" : usecase_schema.dump(usecase)})



###################### POST ENDPOINTS #####################



@app.route('/rexs', methods=['POST'])
def insert_rex():
    data = request.json
    client_name = data['client_name']
    id_solution = data['id_solution']
    id_usecase = data['id_usecase']
    details = data['details']
    client_sector = data['client_sector']
    client_size = data['client_size']
    gains_m = data['gains_m']
    gains_nm = data['gains_nm']
    rex = data['rex']

    if 'id' in list(data.keys()) :
        id = data['id']
        new_rex = Rexs(
            id = id,
            client_name = client_name,
            id_solution = id_solution,
            id_usecase = id_usecase,
            details = details,
            client_sector = client_sector,
            client_size = client_size,
            gains_m = gains_m,
            gains_nm = gains_nm,
            rex = rex)

        db.session.add(new_rex)
        db.session.commit()

    else : 
        new_rex = Rexs(
            client_name = client_name,
            id_solution = id_solution,
            id_usecase = id_usecase,
            details = details,
            client_sector = client_sector,
            client_size = client_size,
            gains_m = gains_m,
            gains_nm = gains_nm,
            rex = rex)

        db.session.add(new_rex)
        db.session.commit()
    return (jsonify({"status" : "ok"}), status.HTTP_200_OK)


@app.route('/solutions', methods=['POST'])
def insert_solution():
    data = request.json
    name_solution = data['name_solution']
    name_developper = data['name_developper']
    summary = data['summary']
    type_solution = data['type_solution']
    technos = data['technos']
    ico = data['ico']
    size = data['size']

    if 'id' in list(data.keys()) :
        id = data['id']
        new_solution = Solutions(
            id = id,
            name_solution = name_solution,
            name_developper = name_developper,
            summary = summary,
            type_solution = type_solution,
            technos = technos,
            ico = ico,
            size = size
        )
        db.session.add(new_solution)
        db.session.commit()

    else :
        new_solution = Solutions(
            name_solution = name_solution,
            name_developper = name_developper,
            summary = summary,
            type_solution = type_solution,
            technos = technos,
            ico = ico,
            size = size
        )
        db.session.add(new_solution)
        db.session.commit()

    return (jsonify({"status" : "ok"}), status.HTTP_200_OK)


@app.route('/usecases', methods=['POST'])
def insert_usecase():
    data = request.json
    name_usecase = data['name_usecase']
    summary = data['summary']
    goals = data['goals']
    target_user = data['target_user']
    department = data['department']
    sector = data['sector']
    if 'id' in list(data.keys()) :
        id = data['id']
        new_usecase = Usecases(
            id = id,
            name_usecase = name_usecase,
            summary = summary,
            goals = goals,
            target_user = target_user,
            department = department,
            sector = sector
        )
        db.session.add(new_usecase)
        db.session.commit() 
    else :
        new_usecase = Usecases(
            name_usecase = name_usecase,
            summary = summary,
            goals = goals,
            target_user = target_user,
            department = department,
            sector = sector
        )
        db.session.add(new_usecase)
        db.session.commit() 
        
    return (jsonify({"status" : "ok"}), status.HTTP_200_OK)



### search endpoints 

@app.route('/solutions/search/<path>', methods=['GET'])
def search_solutions(path):
    path = f'%{path}%' 
    solutions = Solutions.query.filter(Solutions.summary.like(path)).all()
    return jsonify({"data" : solutions_schema.dump(solutions)})


@app.route('/rexs/search/<path>', methods=['GET'])
def search_rexs(path):
    path = f'%{path}%'
    rexs = Rexs.query.filter(Rexs.details.like(path)).all()
    return jsonify({"data" : rexs_schema.dump(rexs)})


@app.route('/usecases/search/<path>', methods=['GET'])
def search_usecases(path):
    path = f'%{path}%'
    usecases = Usecases.query.filter(Usecases.summary.like(path)).all()
    return jsonify({"data" : usecases_schema.dump(usecases)})







############# MAIN ##############

if __name__ == "__main__":
  app.run(host ='0.0.0.0')  


