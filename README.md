# flask_docker

Sandbox project where I implemented an API to query the DB. Simply 3 containers communicating with docker-compose network : one with a flask server, one with a postgresDB instance, and one with an Adminer UI for DB.

## Getting started

Needed : Docker installed on your computer. Basically all.
To launch the server, go to the root folder of this project and launch the platform. 
$ docker-compose -f docker-compose.dev.yml

Server is running, you can now acess the 3 containers through :
- localhost:5000 for flask server.
- localhost:5432 for DB instance.
- localhost:8080 for DB Adminer UI.

