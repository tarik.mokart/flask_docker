from database.models import Rexs, Solutions, Usecases
from flask_marshmallow import Schema

class RexsSchema(Schema):
    class Meta :
        fields = ['client_name', 'id_solution', 'id_usecase', 'details', 'client_sector',
       'client_size', 'gains_m', 'gains_nm', 'rex']
        model = Rexs

class SolutionsSchema(Schema):
    class Meta :
        fields = ['id', 'name_solution', 'name_developper', 'summary', 'type_solution',
       'technos', 'ico', 'size']
        model = Solutions

class UsecasesSchema(Schema):
    class Meta :
        fields = ['id', 'name_usecase', 'summary', 'goals', 'target_user', 'department',
       'sector']
        model = Usecases



rexs_schema = RexsSchema(many=True)
rex_schema = RexsSchema()

solutions_schema = SolutionsSchema(many=True)
solution_schema = SolutionsSchema()

usecases_schema = UsecasesSchema(many=True)
usecase_schema = UsecasesSchema()




