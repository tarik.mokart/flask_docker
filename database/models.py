import flask_sqlalchemy
from sqlalchemy import ForeignKey

db = flask_sqlalchemy.SQLAlchemy()


class Rexs(db.Model) :
    __tablename__ = 'rexs'
    id = db.Column(db.Integer, primary_key = True)
    client_name = db.Column(db.String)
    id_solution = db.Column(db.Integer,ForeignKey('solutions.id'))
    id_usecase = db.Column(db.Integer, ForeignKey('usecases.id'))
    details = db.Column(db.String)
    client_sector = db.Column(db.String)
    client_size = db.Column(db.String)
    gains_m = db.Column(db.String)
    gains_nm = db.Column(db.String)
    rex = db.Column(db.String)



class Solutions(db.Model) :
    __tablename__ = 'solutions'
    id = db.Column(db.Integer,primary_key = True)
    name_solution = db.Column(db.String)
    name_developper = db.Column(db.String)
    summary = db.Column(db.String)
    type_solution = db.Column(db.String)
    technos = db.Column(db.String)
    ico = db.Column(db.String)
    size = db.Column(db.String)
    

class Usecases(db.Model):
    __tablename__ = 'usecases'
    id = db.Column(db.Integer,primary_key = True)
    name_usecase =  db.Column(db.String)
    summary = db.Column(db.String)
    goals =  db.Column(db.String)
    target_user = db.Column(db.String)
    department  = db.Column(db.String)
    sector  = db.Column(db.String)






    
